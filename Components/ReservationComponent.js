import React, { Component } from 'react'
import { View, Text, ScrollView, Switch, Picker, StyleSheet, Modal, Alert } from 'react-native'
import DatePicker from 'react-native-datepicker'
import { Button } from 'react-native-elements';
import { Permissions, Notifications } from 'expo'
import * as Animatable from 'react-native-animatable'
export default class ReservationComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            guests: 1,
            date: '',
            smoking: false,
            showModal: false
        }
    }
    static navigationOptions = {
        title: 'Reserve Table'
    }

    resetForm = () => {
        this.setState({
            guests: 1,
            date: '',
            smoking: false
        })
    }

    async obtainNotificationPermission() {
        let permission = await Permissions.getAsync(Permissions.USER_FACING_NOTIFICATIONS);
        if (permission.status !== 'granted') {
            permission = await Permissions.askAsync(Permissions.USER_FACING_NOTIFICATIONS);
            if (permission.status !== 'granted') {
                Alert.alert('Permission not granted to show notifications');
            }
        }
        return permission
    }
    async presentLocalNotification(date) {
        await this.obtainNotificationPermission();
        Notifications.presentLocalNotificationAsync({
            title: 'Your Reservation',
            body: 'Reservation for' + date + 'requested',
            ios: {
                sound: true
            },
            android: {
                sound: true,
                vibrate: true,
                color: '#512DA8'
            }
        })
    }
    render() {
        return (
            <Animatable.View animation="zoomIn" duration={2000} delay={1000}>
                <View style={styles.formRow}>
                    <Text style={styles.formLabel}>Number of Guests</Text>
                    <Picker
                        style={styles.formItem}
                        selectedValue={this.state.guests}
                        onValueChange={(itemValue, itemIndex) => this.setState({ guests: itemValue })}>
                        <Picker.Item label='1' value='1' />
                        <Picker.Item label='2' value='2' />
                        <Picker.Item label='3' value='3' />
                        <Picker.Item label='4' value='4' />
                        <Picker.Item label='5' value='5' />
                        <Picker.Item label='6' value='6' />
                    </Picker>
                </View>
                <View style={styles.formRow}>
                    <Text style={styles.formLabel}>Smoking/Non-Smoking?</Text>
                    <Switch
                        style={styles.formItem}
                        value={this.state.smoking}
                        trackColor='#512DA8'
                        onValueChange={(value) => this.setState({ smoking: value })}>
                    </Switch>
                </View>
                <View style={styles.formRow}>
                    <Text style={styles.formLabel}>Date and Time</Text>
                    <DatePicker
                        style={{ flex: 2, marginRight: 20 }}
                        date={this.state.date}
                        format=''
                        mode='datetime'
                        placeholder='select date and time'
                        minDate='2017-01-01'
                        confirmBtnText='Confirm'
                        cancelBtnText='Cancel'
                        customStyles={{
                            dateIcon: {
                                position: 'absolute',
                                left: 0,
                                top: 4,
                                marginLeft: 0
                            },
                            dateInput: {
                                marginLeft: 36
                            }
                        }}
                        onDateChange={(date) => { this.setState({ date: date }) }}
                    />
                </View>
                <View style={styles.formRow}>
                    <Button
                        title='Reserve'
                        onPress={() => Alert.alert(
                            'Your Reservation OK?',
                            'Number of Guests:' + this.state.guests + "\n" + 'Smoking?' + this.state.smoking + '\n' + 'Date and Time:' + this.state.date,
                            [{
                                text: 'CANCEL', onPress: () => this.resetForm(), style: 'cancel'
                            },
                            {
                                text: 'OK', onPress: () => {
                                        this.presentLocalNotification(this.state.date),
                                        this.resetForm()
                                }
                            }
                            ],
                            { cancelable: false }
                        )}
                        color='#512DA8'
                        accessibilityLabel="Learn more about this purple button"
                    />
                </View>
            </Animatable.View>

        )
    }
}
const styles = StyleSheet.create({
    formRow: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        margin: 20
    },
    formLabel: {
        flex: 2,
        fontSize: 18
    },
    formItem: {
        flex: 1
    },
    modal: {
        justifyContent: 'center',
        margin: 20
    },
    modalTitle: {
        color: 'white',
        textAlign: 'center',
        marginBottom: 20,
        backgroundColor: '#512DAB',
        fontWeight: 'bold',
        fontSize: 24
    },
    modalText: {
        margin: 10,
        fontSize: 18
    }
})
