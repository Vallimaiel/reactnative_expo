import React, { Component } from 'react'
import { Card,Button,Icon } from 'react-native-elements'
import {MailComposer} from 'expo'
import { View, Text } from 'react-native'
import * as Animatable from 'react-native-animatable'
export default class ContactComponent extends Component {
    sendMail=()=>{
        MailComposer.composeAsync({
            recipients:['dyaneshvarun123@gmail.com','mayil1896@gmail.com'],
            subject:'Enquiry',
            body:'To whom it may concern.sent on react native mail composer api.For test purpose'
        })
    }
    render() {
        return (
            <Animatable.View animation='fadeInDown' duration={2000} delay={1000}>
            <Card
                title={'Contact Information'}>
                <Text style={{ fontWeight: 'bold' }}>
                    121, Clear Water Bay Road {"\n"}{"\n"}
                    Clear Water Bay, Kowloon{"\n"}{"\n"}
                    HONG KONG{"\n"}{"\n"}
                    Tel: +852 1234 5678{"\n"}{"\n"}
                    Fax: +852 8765 4321{"\n"}{"\n"}
                    Email:confusion@food.net{"\n"}
                </Text>
                <Button 
                    title='Send Email' 
                    buttonStyle={{backgroundColor:'#512DA8'}} 
                    icon={<Icon name="envelope-o" type='font-awesome' color='white'/>}
                    onPress={()=>this.sendMail()}
                />
            </Card>
            </Animatable.View>
        )
    }
}