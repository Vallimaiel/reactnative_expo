import React from 'react'
import { View, Text, FlatList, ScrollView, Modal, TouchableOpacity, StyleSheet, Alert, PanResponder,Share } from 'react-native'
import { Card, Icon, Rating, Input } from 'react-native-elements'
import { baseUrl } from '../Shared/baseUrl'
import { connect } from 'react-redux'
import { postFavorite, postComment } from '../redux/ActionCreators'
import * as Animatable from 'react-native-animatable'



const mapStateToProps = state => {
    return {
        dishes: state.dishes,
        comments: state.comments,
        favorites: state.favorites,
    }
}
const mapDispatchToProps = dispatch => ({
    postFavorite: (dishId) => dispatch(postFavorite(dishId)),
    postComment: (dishId, rating, comment, author) => dispatch(postComment(dishId, rating, comment, author))
})

function RenderDish(props) {
    const recognizeDrag = ({ moveX, moveY, dx, dy }) => {
        console.log("......", dx)
        if (dx < -200)      //right to left gesture
        {
            return true
        }
        else {                
            return false
        }
    }
    const recognizeComment = ({ moveX, moveY, dx, dy }) => {
        console.log("......", dx)
        if (dx > 200)      // left to right gesture
        {
            return true
        }
        else {                
            return false
        }
    }

    const shareDish=(title,message,url)=>{
        Share.share({
            title:title ,
            message:title+':'+message+''+url,
            url:url
        },
        {
            dialogTitle:'Share'+title
        })

    }
    handleViewRef = ref => this.view = ref
    const panResponder = PanResponder.create({
        onStartShouldSetPanResponder: (e, gestureState) => {
            return true
        },
        onPanResponderGrant: () => {
            this.view.rubberBand(1000)
                .then(endState => console.log(endState.finished ? 'finished' : 'cancelled'))
        },
        onPanResponderEnd: (e, gestureState) => {
            if (recognizeDrag(gestureState)) {
                Alert.alert(
                    'Add to Favorites',
                    'Are you sure you wish to add ' + props.dishItemSelected.name + 'to your favorites?',
                    [{
                        text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'
                    },
                    {
                        text: 'Ok', onPress: () => props.favorite ? console.log('Already favorite') : props.onPress()
                    }
                    ],
                    { cancelable: false }
                )
                
            }
            if(recognizeComment(gestureState))
            {
                props.displayModal()
                
            }
            return true  
        }
    })
    if (props.dishItemSelected != null) {
        return (
            <Animatable.View animation='fadeInDown' duration={2000} delay={1000} ref={this.handleViewRef} {...panResponder.panHandlers}>
                <Card
                    featuredTitle={props.dishItemSelected.name}
                    image={{ uri: baseUrl + props.dishItemSelected.image }}>
                    <Text>{props.dishItemSelected.description}</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                        <Icon
                            raised
                            reverse
                            name={props.favorite ? 'heart' : 'heart-o'}
                            type='font-awesome'
                            color='#f50'
                            onPress={() => props.favorite ? console.log('Already favorite') : props.onPress()}
                        />
                        <Icon
                            raised
                            reverse
                            name={'pencil'}
                            type='font-awesome'
                            color='#00008b'
                            onPress={() => props.displayModal()}
                        />
                        <Icon
                            raised
                            reverse
                            name={'share'}
                            type='font-awesome'
                            color='#51D2A8'
                            onPress={()=>shareDish(props.dishItemSelected.name,props.dishItemSelected.description,baseUrl+props.dishItemSelected.image)}
                            />
                    </View>
                </Card>
            </Animatable.View>
        )
    }
    else {
        return (<View></View>)
    }
}
function RenderComments(props) {

    const comments = props.commentItemSelected;

    const renderCommentItem = ({ item, index }) => {

        return (
            <View key={index} style={{ margin: 10 }}>
                <Text style={{ fontSize: 14 }}>{item.comment}</Text>
                <Rating
                    count={5}
                    startingValue={item.rating}
                    imageSize={20}
                    style={{ alignItems: 'flex-start' }}
                    readonly />
                <Text style={{ fontSize: 12 }}>{'-- ' + item.author + ', ' + item.date} </Text>
            </View>
        );
    };

    return (
        <View>
            <Animatable.View animation='fadeInUp' delay={1000} duration={2000}>
                <Card title='Comments' >
                    <FlatList
                        data={comments}
                        renderItem={renderCommentItem}
                        keyExtractor={item => item.id.toString()}
                    />
                </Card>
                <Text>{"\n"}{"\n"}</Text>
            </Animatable.View>
        </View>
    );
}

class DishDetailComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showModal: false,
            author: '',
            comment: '',
            ratingFinal: null,

        }
    }
    markFavorite(dishId) {
        this.props.postFavorite(dishId)
    }
    toggleModal() {
        this.setState({
            showModal: !this.state.showModal
        })
    }
    static navigationOptions = {
        title: 'DishDetail'
    }
    ratingCompleted = (rating) => {
        this.setState({ ratingFinal: rating })
        console.log("rating", this.state.ratingFinal)
    }
    addAuthor = (value) => {
        this.setState({ author: value })
    }
    addComment = (value) => {
        this.setState({ comment: value })
    }
    handleComment(dishId, rating, comment, author) {
        if (rating && comment && author) {
            this.props.postComment(dishId, rating, comment, author)
        }
    }
    render() {
        const dishId = this.props.navigation.getParam('dishId', '')

        return (
            this.state.showModal ?

                <Modal visible={this.state.showModal} animationType={'slide'} transparent={false} onRequestClose={() => this.toggleModal()} onDismiss={() => this.toggleModal()}>
                    <Rating
                        type='star'
                        ratingCount={5}
                        imageSize={30}
                        showRating
                        fractions={0}
                        onFinishRating={this.ratingCompleted}
                    />
                    <Input
                        placeholder='Author '
                        value={this.state.author}
                        onChangeText={this.addAuthor}
                        style={{ margin: 10 }}
                        leftIcon={{ type: 'font-awesome', name: 'user-o' }} />
                    <Input
                        placeholder='Comment'
                        style={{ margin: 10 }}
                        value={this.state.comment}
                        onChangeText={this.addComment}
                        leftIcon={{ type: 'font-awesome', name: 'comment-o' }} />
                    <TouchableOpacity style={[styles.button, styles.submitbtn]} onPress={() => this.handleComment(dishId, this.state.ratingFinal, this.state.comment, this.state.author)}><Text style={styles.buttonText}>Submit</Text></TouchableOpacity>
                    <TouchableOpacity style={[styles.button, styles.cancelbtn]} onPress={() => { this.setState({ ratingFinal: '', author: '', comment: '' }), this.toggleModal() }}><Text style={styles.buttonText}>Cancel</Text></TouchableOpacity>
                </Modal>
                :
                <ScrollView>
                    <RenderDish dishItemSelected={this.props.dishes.dishes[+dishId]} favorite={this.props.favorites.some(el => el === dishId)}
                        onPress={() => this.markFavorite(dishId)} displayModal={() => this.toggleModal()} />
                    <RenderComments commentItemSelected={this.props.comments.comments.filter((comment) => comment.dishId === dishId)} />
                </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
        height: 30
    },
    buttonText: {
        fontSize: 16,
        color: 'white'
    },
    submitbtn: {
        backgroundColor: '#00008b',
    },
    cancelbtn: {
        backgroundColor: 'grey',
    }
}
)
export default connect(mapStateToProps, mapDispatchToProps)(DishDetailComponent)