import React from 'react'
import {ActivityIndicator,Text,View,StyleSheet} from 'react-native'

export const Loading=()=>{
    return(
        <View style={styles.loadingView}>
            <ActivityIndicator size='large' color='#512DA8'></ActivityIndicator>
            <Text style={styles.loadingText}>Loading...</Text>
        </View>
    )
}

const styles=StyleSheet.create({
    loadingView:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    loadingText:
    {
        fontSize:14,
        fontWeight:'bold',
        color:'#512DA8'
    }
})