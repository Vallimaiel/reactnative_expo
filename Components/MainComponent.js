import React, { Component } from 'react';
import { View, Platform, TouchableOpacity, StyleSheet, Image, ScrollView, Text,NetInfo,ToastAndroid } from 'react-native'
import { createStackNavigator, createDrawerNavigator, DrawerItems, SafeAreaView } from 'react-navigation'
import MenuComponent from './MenuComponent';
import DishDetailComponent from './DishDetail';
import HomeComponent from './Home';
import ContactComponent from './ContactComponent';
import AboutComponent from './AboutComponent';
import { Icon } from 'react-native-elements'
import { baseUrl } from '../Shared/baseUrl'
import { connect } from 'react-redux'
import { fetchComments, fetchLeaders, fetchPromos, fetchDishes } from '../redux/ActionCreators'
import ReservationComponent from './ReservationComponent';
import Favorites from './FavoriteComponent';
import Login from './LoginComponent'


const mapStateToProps = state => {
  return {

  }
}

const mapDispatchToProps = dispatch => ({
  fetchDishes: () => dispatch(fetchDishes()),
  fetchComments: () => dispatch(fetchComments()),
  fetchPromos: () => dispatch(fetchPromos()),
  fetchLeaders: () => dispatch(fetchLeaders())
})

const CustomDrawerComponent = (props) => (
  <ScrollView>
    <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
      <View style={styles.drawerHeader}>
        <View style={{ flex: 1 }}>
          <Image source={require('./images/logo.png')} style={styles.drawerImage} />
        </View>
        <View style={{ flex: 2 }}>
          <Text style={styles.drawerHeaderText}>Ristorante Con Fusion</Text>
        </View>
      </View>
      <DrawerItems {...props} />

    </SafeAreaView>
  </ScrollView>
)

const LoginNavigator = createStackNavigator({
  Login: { screen: Login },

},
  {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: '#512DA8'
      },
      headerTitleStyle:
      {
        color: '#fff'
      },
      headerTintColor: '#fff',
      headerLeft: (
        <TouchableOpacity style={{ padding: 10 }} onPress={navigation.toggleDrawer}>
          <Icon
            name="menu"
            size={24}
            color='white'
          />
        </TouchableOpacity>
      ),
    })
  })
const HomeNavigator = createStackNavigator({
  Home: { screen: HomeComponent },

},
  {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: '#512DA8'
      },
      headerTitleStyle:
      {
        color: '#fff'
      },
      headerTintColor: '#fff',
      headerLeft: (
        <TouchableOpacity style={{ padding: 10 }} onPress={navigation.toggleDrawer}>
          <Icon
            name="menu"
            size={24}
            color='white'
          />
        </TouchableOpacity>
      ),
    })
  })

const AboutUsNavigator = createStackNavigator({
  About: { screen: AboutComponent },

},
  {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: '#512DA8'
      },
      headerTitleStyle:
      {
        color: '#fff'
      },
      headerTintColor: '#fff',
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.toggleDrawer()} style={{ padding: 10 }}>
          <Icon name='menu' size={24} color='white' />
        </TouchableOpacity>
      )
    })
  })

const MenuNavigator = createStackNavigator({
  Menu: { screen: MenuComponent },
  DishDetail: { screen: DishDetailComponent }
},
  {
    initialRouteName: 'Menu',
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: '#512DA8'
      },
      headerTitleStyle:
      {
        color: '#fff'
      },
      headerTintColor: '#fff',
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.toggleDrawer()} style={{ padding: 10 }}>
          <Icon name='menu' size={24} color='white' />
        </TouchableOpacity>
      )
    })
  })

const ContactUsNavigator = createStackNavigator({
  Contact: { screen: ContactComponent },

},
  {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: '#512DA8'
      },
      headerTitleStyle:
      {
        color: '#fff'
      },
      headerTintColor: '#fff',
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.toggleDrawer()} style={{ padding: 10 }}>
          <Icon name="menu" size={24} color='white' />
        </TouchableOpacity>
      )
    })
  })
  const FavoritesNavigator = createStackNavigator({
    Favorites: { screen: Favorites },
  
  },
    {
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#512DA8'
        },
        headerTitleStyle:
        {
          color: '#fff'
        },
        headerTintColor: '#fff',
        headerLeft: (
          <TouchableOpacity onPress={() => navigation.toggleDrawer()} style={{ padding: 10 }}>
            <Icon name="menu" size={24} color='white' />
          </TouchableOpacity>
        )
      })
    })



const ReservationNavigator = createStackNavigator({
  Reservation: { screen: ReservationComponent },

},
  {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: '#512DA8'
      },
      headerTitleStyle:
      {
        color: '#fff'
      },
      headerTintColor: '#fff',
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.toggleDrawer()} style={{ padding: 10 }}>
          <Icon name="menu" size={24} color='white' />
        </TouchableOpacity>
      )
    })
  })


const MainNavigator = createDrawerNavigator({
  Login: {
    screen: LoginNavigator,
    navigationOptions: {
      drawerLabel: 'Login',
      drawerIcon: ({ tintColor }) => (
        <Icon
          name='sign-in'
          type='font-awesome'
          size={24}
          color={tintColor} />
      )
    }
  },
  Home: {
    screen: HomeNavigator,
    navigationOptions: {
      drawerLabel: 'Home',
      drawerIcon: ({ tintColor }) => (
        <Icon
          name='home'
          type='font-awesome'
          size={24}
          color={tintColor} />
      )
    }
  },
  About: {
    screen: AboutUsNavigator,
    navigationOptions: {
      drawerLabel: 'About Us',
      drawerIcon: ({ tintColor }) => (
        <Icon
          name='info-circle'
          type='font-awesome'
          size={24}
          color={tintColor} />
      )
    }
  },
  Menu: {
    screen: MenuNavigator,
    navigationOptions: {
      drawerLabel: 'Menu',
      drawerIcon: ({ tintColor }) => (
        <Icon
          name='list'
          type='font-awesome'
          size={24}
          color={tintColor} />
      )
    }
  },
  Contact: {
    screen: ContactUsNavigator,
    navigationOptions: {
      drawerLabel: 'Contact Us',
      drawerIcon: ({ tintColor }) => (
        <Icon
          name='address-card'
          type='font-awesome'
          size={24}
          color={tintColor} />
      )
    }
  },
  Favorites:
  {
    screen: FavoritesNavigator,
    navigationOptions: {
      drawerLabel: 'My Favorites',
      drawerIcon: ({ tintColor, focused }) => (
        <Icon
          name='heart'
          type='font-awesome'
          size={24}
          iconStyle={{ color: tintColor }}
        />
      ),
    }
  },
  Reservation: {
    screen: ReservationNavigator,
    navigationOptions: {
      drawerLabel: 'Reserve Table',
      drawerIcon: ({ tintColor }) => (
        <Icon
          name='cutlery'
          type='font-awesome'
          size={24}
          color={tintColor} />
      )
    }
  }
},
  {
    initialRouteName:'Home',
    drawerBackgroundColor: '#D1C4E9',
    contentComponent: CustomDrawerComponent
  });

class Main extends Component {
  componentDidMount() {
    this.props.fetchDishes();
    this.props.fetchComments();
    this.props.fetchPromos();
    this.props.fetchLeaders();
    NetInfo.getConnectionInfo()
      .then((connectionInfo)=>{
        ToastAndroid.show('Initial Network Connectivity Type:'+connectionInfo.type+',effectiveType:'+connectionInfo.effectiveType,ToastAndroid.LONG)
      });
      NetInfo.addEventListener('connectionChange',this.handleConnectivityChange)
  }
  componentWillUnmount()
  {
    NetInfo.removeEventListener('connectionChange',this.handleConnectivityChange)
  }
  handleConnectivityChange=(connectionInfo)=>{
    switch(connectionInfo.type)
    {
      case 'none':
          ToastAndroid.show('You are now offline!',ToastAndroid.LONG);
          break;
     case 'wifi':
          ToastAndroid.show('You are now connected to wifi!',ToastAndroid.LONG);
          break;
      case 'cellular':
          ToastAndroid.show('You are now connected to cellular!',ToastAndroid.LONG);
          break;
      case 'unknown':
          ToastAndroid.show('You now have unknown connection!',ToastAndroid.LONG);
          break;
      default:
          break;
    }

  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <MainNavigator />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  drawerHeader: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#512DA8',
    alignItems: 'center',
    justifyContent: 'center',
    height: 140
  },
  drawerImage: {
    width: 80,
    height: 60,
    margin: 10
  },
  drawerHeaderText: {
    fontWeight: 'bold',
    fontSize: 24,
    color: 'white'
  }

})

export default connect(mapStateToProps, mapDispatchToProps)(Main)

